const { 
		URL, 
		SNAP_DIR, 
		SNAP_DAILY, 
		SNAP_HOURLY,
		ACCOUNT_ID,
		PASSWORD

	} = require('./constants');


var fs = require('fs');
var htmlToJson = require('html-to-json');


const Webdriver = require('selenium-webdriver');
const SeleniumServer = require("selenium-webdriver/remote").SeleniumServer;
const by = require('selenium-webdriver').By; 
const until = require('selenium-webdriver').until;



const browser = Webdriver.Capabilities.chrome();

const driver = new Webdriver
                    .Builder()
                    .withCapabilities(browser)
                    .build();

let snap_dir = SNAP_DIR;
let snapshot_daily = snap_dir + SNAP_DAILY;
let snapshot_hourly = snapshot_daily + SNAP_HOURLY;

let SERVICE_URL = 'https://service.ringcentral.com/login';

/*
	node serve.js ACCOUNT_ID PASSWORD
*/
// If use native php for executing this command
// let ACCOUNT_ID = process.argv[2];
// let PASSWORD = process.argv[3];

driver
    .get(URL)
    .then((data) => {
    	return driver.wait(until.urlContains(SERVICE_URL), 
                    20000, 
                    'FAIL| Unable to access live.ringcentral.com.| trial:3');
    }).then(() => {
        return driver.wait(until.elementLocated(by.className('btn btn-primary btn-block')), 
                    15000, 
                    'FAIL| Unable to access live.ringcentral.com.| trial:3');
    }).then(() => {

    		driver.findElement(by.id('credential')).clear();
        	driver.findElement(by.id('credential')).sendKeys(ACCOUNT_ID);
        	driver.findElement(by.tagName('button')).sendKeys(Webdriver.Key.ENTER);

    	return driver.wait(until.elementLocated(by.name('Password')), 
                    10000, 
                    'FAIL| Issue encountered at the login.| trial:3');
        
    }).then(() => {

    	var form = driver.findElement(by.tagName("form"));
    		form.findElement(by.id('password')).clear();
    		
    		setTimeout(function() {
    			form.findElement(by.id('password')).sendKeys(PASSWORD);
			}, 1000);
    		setTimeout(function() {
    			form.submit();
			}, 1000);
        return driver.wait(until.urlContains(URL), 
                    10000, 
                    'FAIL| Unable to access live.ringcentral.com.| trial:3');
    }).then(() => {
    	return driver.wait(until.elementLocated(by.className('rc-icon rc-icon-logout rc-icon-left')), 
                    10000, 
                    'FAIL| Unable to access live.ringcentral.com.| trial:3');
    }).then(() => {

    	var selector = driver.findElement(by.id("dashboardSelector"));
    		selector.click();

        return driver.wait(until.elementLocated(by.id("dashboardSelector")), 
                    10000, 
                    'FAIL| Unable to load test report.| trial 3');

    }).then(() => {

    		var widgets_class = [
    				"queue-details-widget", "service-level-widget",
    				"agent-count-widget", "call-volume-widget",
    				"queue-calls-widget", "agent-details-widget",
    				"contact-details-widget", "queue-monitor-widget"
    			];

    		widgets_class.forEach(function(widget){
				return driver.wait(until.elementLocated(by.className(widget)), 3000, 'FAIL| Unable to load test report.| trial 3');
			});

	}).then(() => {
		return driver.wait(until.elementLocated(by.xpath("//span[contains(text(), 'Billing Queue')]")), 3000, 'FAIL| Issue encountered verifying widgets.| trial 3');
	}).then(() => {

		setTimeout(function() {
			return driver.findElement(by.xpath("//*[@id='app']/div/div/div/div[4]/div[1]/div/div[@data-test-automation-id='QueueWidget']")).getAttribute('innerHTML')
				.then(function(data) {
					
					var promise = htmlToJson.parse(data, {
					  
					  'queue_monitors': function ($doc) {

					  		let available = parseInt($doc.find("[data-test-automation-id='agents-available']").text());
					  		let talking = parseInt($doc.find("[data-test-automation-id='agents-talking']").text());
					  		let unavailable = parseInt($doc.find("[data-test-automation-id='agents-unavailable']").text());
					  		let total = available + talking + unavailable;

						  	var value = {
						  		'agents_available': available,
						  		'agents_talking': talking,
						  		'agents_unavailable': unavailable,
						  		'total': total
						  	};

						    return value; 
						}

					}, function (err, queue_monitors) {

						driver.findElement(by.xpath("//*[@id='app']/div/div/div/div[4]/div[1]/div/div[@data-test-automation-id='AgentStatsWidget']")).getAttribute('innerHTML')
							.then(function(data) {
								
								var promise = htmlToJson.parse(data, {
								  
								  'agents_count': function ($doc) {

								  		let available = parseInt($doc.find("[data-test-automation-id='ValueBar1']").text());
								  		let talking = parseInt($doc.find("[data-test-automation-id='ValueBar2']").text());
								  		let unavailable = parseInt($doc.find("[data-test-automation-id='ValueBar3']").text());
								  		let total = available + talking + unavailable;

									  	var value = {
									  		'agents_available': available,
									  		'agents_talking': talking,
									  		'agents_unavailable': unavailable,
									  		'total': total
									  	};

									    return value; 
									},
									'queue_monitors': queue_monitors.queue_monitors
								});

								promise.then(function(data){
									
									let queue_monitors = data.queue_monitors;
									let agents_count = data.agents_count;

									if(
										(queue_monitors.agents_available === agents_count.agents_available && 
										queue_monitors.agents_talking === agents_count.agents_talking && 
										queue_monitors.agents_unavailable === agents_count.agents_unavailable)
									) {

										var queue_total = queue_monitors.total;
										var agents_total = agents_count.total;

										driver.findElement(by.xpath("//*[@id='app']/div/div/div/div[4]/div[1]/div/div[@data-test-automation-id='AgentDetailsWidget']/div/div/div[2]/div/div[3]/div/div[1]/span/span")).getAttribute('innerHTML')
											.then(function(data) {

												let agent_details_total = parseInt(data.substring(6, 9));

												if(!(agent_details_total === queue_total && agent_details_total === agents_total)) {
													throw new Error('FAIL| Issue encountered verifying widget values.| trial 2');
												}

											});	

									} else {
										throw new Error('FAIL| Issue encountered verifying widget values.| trial 3');
									}

								});

							});	

					});
				});	

		}, 5000);

	 }).then(() => {
	 	console.log('SUCCESS| Live reports web page test ok.| trial:$x');
	 }).catch((err) => {
    	console.log(err);
    });
