require('dotenv').config();
var date = require('date-and-time');

let now = new Date();

module.exports = {
  ACCOUNT_ID: process.env.ACCOUNT,
  PASSWORD: process.env.PASSWORD,
  URL: process.env.URL,
  PLATFORM: process.platform,
  SNAP_DIR: process.env.SNAPSHOTS_PATH,
  SNAP_DAILY:  date.format(now, 'MM-DD-YY') + '/',
  SNAP_HOURLY: date.format(now, 'HH') + '/'
}