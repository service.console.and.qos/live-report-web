const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const By = webdriver.By;
const until = webdriver.until;
const fs = require('fs');
const withGui = false;
const WidgetChecker = require('./../src/WidgetChecker.js');

const commandLineArgs = require('command-line-args');
const optionDefinitions = [
    {
        name: 'url', alias: 'u', type: String
    },
    {
        name: 'account', alias: 'a', type: String
    },
    {
        name: 'password', alias: 'p', type: String
    },
    {
        name: 'country', alias: 'c', type: String
    },
];

const cliOptions = commandLineArgs(optionDefinitions);
console.log('options', cliOptions);
const url = cliOptions['url'];
const account = cliOptions['account'];
/*
const password = cliOptions['password'];
*/

/*
const url = 'https://live.ringcentral.ca';
const account = '12093258461';
*/
let password;
if (url === "https://live-officeathand.att.com") {
  password = 'Password10';
} else {
  password = 'Testing!123';
}

(async () => {
    const options = new chrome.Options();

    if (!withGui) {
        options.addArguments('headless');
        options.addArguments('disable-gpu');
    }

    const path = require('chromedriver').path;
    const service = new chrome.ServiceBuilder(path).build();
    chrome.setDefaultService(service);

    const driver = new webdriver.Builder()
        .forBrowser('chrome')
        .withCapabilities(webdriver.Capabilities.chrome())
        .setChromeOptions(options)
        .build();

    try {
        //driver.manage().window().setRect({width: 900, height: 900});

        await driver.get(url)

        console.log('trying to render first login form next button');

        await driver.wait(until.elementLocated(By.name('credential')),
                    30000,
                    'Unable to render first login form next button.');

        console.log('first login form submit...');

        await driver.findElement(By.id('credential')).clear();
        await driver.findElement(By.id('credential')).sendKeys(account);
        await driver.findElement(By.tagName('button')).sendKeys(webdriver.Key.ENTER);

        console.log('waiting for login form password');

        await driver.wait(until.elementLocated(By.name('Password')),
                    10000,
                    'Unable to render login form password field.');

        console.log('filling login form password');

    	  const form = driver.findElement(By.tagName("form"));
        await form.findElement(By.id('password')).clear();
        await form.findElement(By.id('password')).sendKeys(password);

        console.log('submitting login form password');

        await driver.findElement(By.css("[data-test-automation-id='signInBtn']"))
                    .sendKeys(webdriver.Key.ENTER);

        console.log('waiting for live.ringcentral.com redirect');

        await driver.wait(until.elementLocated(By.id("dashboardSelector")),
                    30000,
                    'Unable to access live.ringcentral.com.');

        console.log('live.ringcentral.com rendered');

        await driver.sleep(20000);

        let xpaths;

        if (url === "https://live-officeathand.att.com") {
            xpaths = WidgetChecker.xpathsAtt();
        } else {
            xpaths = WidgetChecker.xpaths();
        }

        console.log('all widgets are successfully checked');
        console.log('checking billing queue');
        if (url === "https://live.ringcentral.ca" || url === "https://live.ringcentral.co.uk") {
            console.log('SUCCESS');
	    return true;
	}
        const queueAgentsAvailable = await driver
                                .findElement(By.xpath(xpaths.QUEUE_MONITOR_AGENTS_AVAILABLE_XPATH))
                                .getAttribute('innerHTML');
        const queueAgentsTalking = await driver
                                .findElement(By.xpath(xpaths.QUEUE_MONITOR_AGENTS_TALKING_XPATH))
                                .getAttribute('innerHTML');
        const queueAgentsUnavailable = await driver
                            .findElement(By.xpath(xpaths.QUEUE_MONITOR_AGENTS_UNAVAILABLE_XPATH))
                            .getAttribute('innerHTML');
        const barAgentsAvailable = await driver
                                .findElement(By.xpath(xpaths.BAR_AGENTS_AVAILABLE_XPATH))
                                .getAttribute('innerHTML');
        const barAgentsTalking = await driver
                                .findElement(By.xpath(xpaths.BAR_AGENTS_TALKING_XPATH))
                                .getAttribute('innerHTML');
        const barAgentsUnavailable = await driver
                                .findElement(By.xpath(xpaths.BAR_AGENTS_UNAVAILABLE_XPATH))
                                .getAttribute('innerHTML');

        console.log(queueAgentsAvailable, barAgentsAvailable);
        console.log(queueAgentsTalking, barAgentsTalking);
        console.log(queueAgentsUnavailable, barAgentsUnavailable);
        let isSuccess = true;
        if (queueAgentsAvailable != barAgentsAvailable) {
            isSuccess = false;
        }
        if (queueAgentsTalking != barAgentsTalking) {
            isSuccess = false;
        }
        if (queueAgentsUnavailable != barAgentsUnavailable) {
            isSuccess = false;
        }
        console.log('isSuccess', isSuccess);
        if (isSuccess) {
          console.log('SUCCESS');
        } else {
          console.log('FAIL');
        }
    } catch (err) {
        console.log('err', err);
    } finally {
        console.log('driver quit');
        driver.quit();
    }
})();
