const by = require('selenium-webdriver').By; 

function getInnerHtml(driver, xpath) {
  return driver
          .findElement(by.xpath(xpath))
          .getAttribute('innerHTML');
}

module.exports = getInnerHtml;
