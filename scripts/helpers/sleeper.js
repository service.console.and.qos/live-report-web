function sleeper(time, param) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      return resolve(param);
    }, time);
  })
}

module.exports = sleeper;
