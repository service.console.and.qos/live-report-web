function clearOutTags(htmlString) {
  return htmlString.replace(/<(?:.|\n)*?>/gm, '');
}

module.exports = clearOutTags;
