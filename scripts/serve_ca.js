const {
	URL,
	SNAP_DIR,
	SNAP_DAILY,
	SNAP_HOURLY,
	ACCOUNT_ID,
	PASSWORD
} = require('./constants');

const Webdriver = require('selenium-webdriver');
const SeleniumServer = require("selenium-webdriver/remote").SeleniumServer;
const by = require('selenium-webdriver').By;
const until = require('selenium-webdriver').until;
const commandLineArgs = require('command-line-args');
const chrome = require('selenium-webdriver/chrome');

const sleeper = require('./helpers/sleeper.js');
const getInnerHtml = require('./helpers/getInnerHtml.js');
const clearOutTags = require('./helpers/clearOutTags.js');
const WidgetCount = require('./../src/WidgetCount.js');
const WidgetChecker = require('./../src/WidgetChecker.js');

const browser = Webdriver.Capabilities.chrome();
browser.set(Webdriver.Capability.ACCEPT_SSL_CERTS, true);
browser.set(Webdriver.Capability.SECURE_SSL, false);
browser.set("chrome.cli.args",
	["--web-security=no",
		"--ssl-protocol=any",
		"--ignore-ssl-errors=yes"]);

const optionsC = new chrome.Options();
optionsC.addArguments('headless');
optionsC.addArguments('disable-gpu');
optionsC.addArguments('no-sandbox');
optionsC.addArguments('disable-dev-shm-usage');
const path = require('chromedriver').path;
const service = new chrome.ServiceBuilder(path).build();
chrome.setDefaultService(service);

const driver = new Webdriver.Builder()
	.forBrowser('chrome')
	.withCapabilities(Webdriver.Capabilities.chrome())
	.setChromeOptions(optionsC)
	.build();

const optionDefinitions = [
	{
		name: 'url', alias: 'u', type: String
	},
	{
		name: 'account', alias: 'a', type: String
	},
	{
		name: 'password', alias: 'p', type: String
	},
	{
		name: 'country', alias: 'c', type: String
	},
];

const options = commandLineArgs(optionDefinitions);

console.log(options);

const snapshotHourly = SNAP_DIR + SNAP_DAILY + SNAP_HOURLY;

const queueMonitor = new WidgetCount();
const supportMonitor = new WidgetCount();
const agentsCount = new WidgetCount();

const ca_xpaths = WidgetChecker.ca_xpaths();

const SERVICE_URL = 'https://service.ringcentral.com/login';

console.log('initializing');
driver
	.get(options['url'])
	.then( () => {
		console.log('trying to render first login form next button');
		return driver.wait(until.elementLocated(by.className('btn btn-primary btn-block')),
			50000,
			'Unable to render first login form next button.');

	})
	.then( () => {
		console.log('first login form submit...');
		driver.findElement(by.id('credential')).clear();
		driver.findElement(by.id('credential')).sendKeys(options['account']);
		driver.findElement(by.tagName('button')).sendKeys(Webdriver.Key.ENTER);
		console.log('waiting for login form password');
		return driver.wait(until.elementLocated(by.name('Password')),
			30000,
			'Unable to render login form password field.');

	})
	.then( () => {
		console.log('filling login form password');
		const form = driver.findElement(by.tagName("form"));
		form.findElement(by.id('password')).clear();
		return sleeper(1000, form);
	})
	.then( form => {
		//form.findElement(by.id('password')).sendKeys(options['password']);
		form.findElement(by.id('password')).sendKeys("Testing!123");
		return sleeper(1000, form);
	})
	.then( form => {
		console.log('submitting login form password');
		form.submit();
		console.log('waiting for live.ringcentral.ca redirect');
		return driver.wait(until.urlContains(options['url']),
			50000,
			'Unable to access live.ringcentral.ca.');
	})
	.then( () => {
		console.log('rendering live.ringcentral.ca');
		return driver.wait(until.elementLocated(by.className('rc-icon rc-icon-logout rc-icon-left')),
			30000,
			'Unable to render live.ringcentral.ca icons.');
	})
	.then( () => {
		console.log('live.ringcentral.ca rendered');
		var selector = driver.findElement(by.id("dashboardSelector"));
		selector.click();
		console.log('dashboard selector click test');
		return driver.wait(until.elementLocated(by.id("dashboardSelector")),
			30000,
			'Unable to load test report dashboard.');

	})
	.then( () => {
		console.log('live.ringcentral.ca rendered');
		var selector = driver.findElement(by.css("[data-test-automation-id='dashboard-257033016-445421016-8b3fffc4-1152-4266-b433-2c31aa53c077']"));
		console.log('dashboard selector click test');
		return driver.wait(until.elementLocated(by.css("[data-test-automation-id='dashboard-257033016-445421016-8b3fffc4-1152-4266-b433-2c31aa53c077']")),
			30000,
			'Unable to load test report dashboard.');
	})
	.then( () => {
		console.log('checking [Queue] in Queue Details widget');
		return driver.wait(until.elementLocated(by.xpath("//*[@data-test-automation-id='QueueDetailsWidget-grid']//span[contains(.,'Queue')]")),
			30000,
			'Unable to locate [Queue] in Queue Details widget.');

	})
	.then( () => {
		console.log('all widgets are successfully checked');
	})
	.then( () => sleeper(7000))
	.then( () => getInnerHtml(driver, ca_xpaths.QUEUE_MONITOR_AGENTS_AVAILABLE_XPATH))
	.then( innerHtml => queueMonitor.agentsAvailable = clearOutTags(innerHtml))

	.then( () => getInnerHtml(driver, ca_xpaths.QUEUE_MONITOR_AGENTS_TALKING_XPATH))
	.then( innerHtml => queueMonitor.agentsTalking = clearOutTags(innerHtml))

	.then( () => getInnerHtml(driver, ca_xpaths.QUEUE_MONITOR_AGENTS_UNAVAILABLE_XPATH))
	.then( innerHtml => queueMonitor.agentsUnavailable = clearOutTags(innerHtml))

	.then( () => getInnerHtml(driver, ca_xpaths.SUPPORT_MONITOR_AGENTS_AVAILABLE_XPATH))
	.then( innerHtml => supportMonitor.agentsAvailable = clearOutTags(innerHtml))

	.then( () => getInnerHtml(driver, ca_xpaths.SUPPORT_MONITOR_AGENTS_TALKING_XPATH))
	.then( innerHtml => supportMonitor.agentsTalking = clearOutTags(innerHtml))

	.then( () => getInnerHtml(driver, ca_xpaths.SUPPORT_MONITOR_AGENTS_UNAVAILABLE_XPATH))
	.then( innerHtml => supportMonitor.agentsUnavailable = clearOutTags(innerHtml))

	.then( () => getInnerHtml(driver, ca_xpaths.BAR_AGENTS_AVAILABLE_XPATH))
	.then( innerHtml => agentsCount.agentsAvailable = clearOutTags(innerHtml))

	.then( () => getInnerHtml(driver, ca_xpaths.BAR_AGENTS_TALKING_XPATH))
	.then( innerHtml => agentsCount.agentsTalking = clearOutTags(innerHtml))

	.then( () => getInnerHtml(driver, ca_xpaths.BAR_AGENTS_UNAVAILABLE_XPATH))
	.then( innerHtml => agentsCount.agentsUnavailable = clearOutTags(innerHtml))

	.then( () => (new WidgetChecker(queueMonitor, supportMonitor, agentsCount)).areAllEqual())
	.then( result => console.log(result ? 'SUCCESS' : 'FAIL'))

	.then( () => {
		console.log(agentsCount.all());
		console.log(queueMonitor.all());
		console.log(supportMonitor.all());
	})
	.catch( err => console.log(err))
	.then(() => driver.quit());
