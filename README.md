# Live Reports Web (LRW) Monitoring Module

Module for performing test to RingCentral's Live Reports Web application 
(https://live.ringcentral.com). 

The reponsiblities of this module are as follows:
- perform browser tests including login action and dashboard checks
- determine whether the test passed or failed
- capability to perform API requests to UPTC site to fetch data relevant to performing LRW tests
  such as account details, test timings, and on-demand test requests
- capability to send test results which includes logs, dates and print screens to UPTC site
  through REST API

## References
- for more details, check this [Reference Wiki](https://wiki.ringcentral.com/display/OPS/18.9+Live+Reports+Web%28LRW%29+Monitoring+at+UPTC)

## Setup
- Refer to "uptc-setup" repository for a full detail on integrating with UPTC site that is capable

  on communicating through the UPTC REST API.
- The setup that is described here is as an independent setup only for this module.

### Requirements
- NodeJS 10 with NPM and ideally installed througn NVM
- Chromedriver
- access to uptc2 Gitlab group and repositories

### Installation

it is assumed that you already have a NodeJS environment

#### Setup codebase

ensure that the env.example which will be the .env file contains the correct parameters

```
git clone git@git.ringcentral.com:uptc2/live_report_web.git
cd live_report_web
npm install
cp env.example .env
```

#### Install Chromedrive
```
sudo su
wget https://chromedriver.storage.googleapis.com/2.40/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
sudo mv chromedriver /usr/local/bin/chromedriver
sudo chown root:root /usr/local/bin/chromedriver
sudo chmod 0755 /usr/local/bin/chromedriver
```

#### Test If Working

you should see a browser openning in "https://live.ringcentral.com"

```
node scripts/serve.js
```

#### Profit!!!
