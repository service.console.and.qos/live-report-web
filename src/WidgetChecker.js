class WidgetChecker {
  constructor(queueMonitor, supportMonitor, agentsCount) {
    this.queueMonitor = queueMonitor;
    this.supportMonitor = supportMonitor;
    this.agentsCount = agentsCount;
  }

  areAgentsAvailableEqual() {
    return this.queueMonitor.agentsAvailable === this.supportMonitor.agentsAvailable &&
            this.supportMonitor.agentsAvailable === this.agentsCount.agentsAvailable;
  }

  areTalkingAvailableEqual() {
    return this.queueMonitor.agentsTalking === this.supportMonitor.agentsTalking &&
            this.supportMonitor.agentsTalking === this.agentsCount.agentsTalking;
  }

  areAgentsUnavailableEqual() {
    return this.queueMonitor.agentsUnavailable === this.supportMonitor.agentsUnavailable &&
            this.supportMonitor.agentsUnavailable === this.agentsCount.agentsUnavailable;
  }

  areAllEqual() {
    return this.areAgentsAvailableEqual() && this.areTalkingAvailableEqual()
            && this.areAgentsUnavailableEqual();
  }

  static xpaths() {
    return {
      QUEUE_MONITOR_AGENTS_AVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget1'][1]//*[@data-test-automation-id='agents-available']//span[1]",
      QUEUE_MONITOR_AGENTS_TALKING_XPATH: "//*[@data-test-automation-id='QueueWidget1'][1]//*[@data-test-automation-id='agents-talking']//span[1]",
      QUEUE_MONITOR_AGENTS_UNAVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget1'][1]//*[@data-test-automation-id='agents-unavailable']//span[1]",
      SUPPORT_MONITOR_AGENTS_AVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget'][1]//*[@data-test-automation-id='agents-available']",
      SUPPORT_MONITOR_AGENTS_TALKING_XPATH:  "//*[@data-test-automation-id='QueueWidget'][1]//*[@data-test-automation-id='agents-talking']",
      SUPPORT_MONITOR_AGENTS_UNAVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget'][1]//*[@data-test-automation-id='agents-unavailable']",
      BAR_AGENTS_AVAILABLE_XPATH: "//*[@data-test-automation-id='ValueBar1']",
      BAR_AGENTS_TALKING_XPATH: "//*[@data-test-automation-id='ValueBar2']",
      BAR_AGENTS_UNAVAILABLE_XPATH: "//*[@data-test-automation-id='ValueBar3']",
    };
  }

  static xpathsAU() {
    return {
      QUEUE_MONITOR_AGENTS_AVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget0'][1]//*[@data-test-automation-id='agents-available']//span[1]",
      QUEUE_MONITOR_AGENTS_TALKING_XPATH: "//*[@data-test-automation-id='QueueWidget0'][1]//*[@data-test-automation-id='agents-talking']//span[1]",
      QUEUE_MONITOR_AGENTS_UNAVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget0'][1]//*[@data-test-automation-id='agents-unavailable']//span[1]",
      SUPPORT_MONITOR_AGENTS_AVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget'][1]//*[@data-test-automation-id='agents-available']",
      SUPPORT_MONITOR_AGENTS_TALKING_XPATH:  "//*[@data-test-automation-id='QueueWidget'][1]//*[@data-test-automation-id='agents-talking']",
      SUPPORT_MONITOR_AGENTS_UNAVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget'][1]//*[@data-test-automation-id='agents-unavailable']",
      BAR_AGENTS_AVAILABLE_XPATH: "/html/body/div/div[1]/div/div/div[4]/div/div[1]/div/section[4]/div/div/div/div/div[2]/div/div/div[2]/span",
      BAR_AGENTS_TALKING_XPATH: "/html/body/div/div[1]/div/div/div[4]/div/div[1]/div/section[4]/div/div/div/div/div[2]/div/div/div[4]/span",
      BAR_AGENTS_UNAVAILABLE_XPATH: "//*[@data-test-automation-id='ValueBar4']",
    };
  }

  static xpathsAtt() {
    return {
      QUEUE_MONITOR_AGENTS_AVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget0'][1]//*[@data-test-automation-id='agents-available']//span[1]",
      QUEUE_MONITOR_AGENTS_TALKING_XPATH: "//*[@data-test-automation-id='QueueWidget0'][1]//*[@data-test-automation-id='agents-talking']//span[1]",
      QUEUE_MONITOR_AGENTS_UNAVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget0'][1]//*[@data-test-automation-id='agents-unavailable']//span[1]",
      SUPPORT_MONITOR_AGENTS_AVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget'][1]//*[@data-test-automation-id='agents-available']",
      SUPPORT_MONITOR_AGENTS_TALKING_XPATH:  "//*[@data-test-automation-id='QueueWidget'][1]//*[@data-test-automation-id='agents-talking']",
      SUPPORT_MONITOR_AGENTS_UNAVAILABLE_XPATH: "//*[@data-test-automation-id='QueueWidget'][1]//*[@data-test-automation-id='agents-unavailable']",
      BAR_AGENTS_AVAILABLE_XPATH: "//*[@data-test-automation-id='AgentStatsWidget3']//*[@data-test-automation-id='ValueBar1']",
      BAR_AGENTS_TALKING_XPATH: "//*[@data-test-automation-id='ValueBar2']",
      BAR_AGENTS_UNAVAILABLE_XPATH: "//*[@data-test-automation-id='ValueBar3']",
    };
  }
}

module.exports = WidgetChecker;
