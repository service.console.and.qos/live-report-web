class WidgetCount {
  constructor() {
    this.agentsAvailable = null;
    this.agentsTalking = null;
    this.agentsUnavailable = null;
  }

  all() {
    return {
      agentsAvailable: this.agentsAvailable,
      agentsTalking: this.agentsTalking,
      agentsUnavailable: this.agentsUnavailable
    }
  }
}

module.exports = WidgetCount;
